package com.inzynierka.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inzynierka.repository.RoleRepository;
import com.inzynierka.repository.UserRepository;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.UserRole;

@Service
@Transactional
public class UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    @Autowired
    private AccountService accountService;

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    private User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User createUser(User user) {
        Set<UserRole> userRoles = saveUserRoles(user);

        User localUser = userRepository.findByUsername(user.getUsername());

        if (localUser != null) {
            LOG.info("User with username {} already exist. Nothing will be done. ", user.getUsername());
        } else {
            localUser = buildAndSaveUser(user, userRoles);
        }

        return localUser;
    }

    private User buildAndSaveUser(User user, Set<UserRole> userRoles) {
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);

        user.getUserRoles().addAll(userRoles);

        user.setPrimaryAccount(accountService.createPrimaryAccount());
        user.setSavingsAccount(accountService.createSavingsAccount());

        return userRepository.save(user);
    }

    private Set<UserRole> saveUserRoles(User user){
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(new UserRole(user, roleRepository.findByName("USER")));

        userRoles.stream().forEach(userRole -> roleRepository.saveAndFlush(userRole.getRole()));

        return userRoles;
    }

    public boolean checkUserExists(String username, String email){
        return checkUsernameExists(username) || checkEmailExists(username);
    }

    public boolean checkUsernameExists(String username) {
        return null != findByUsername(username);

    }
    
    public boolean checkEmailExists(String email) {
        return null != findByEmail(email);

    }

    public void saveUser (User user) {
        userRepository.save(user);
    }
    
    public List<User> findUserList() {
        return userRepository.findAll();
    }
}
